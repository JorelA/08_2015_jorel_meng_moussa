#ifndef MARTIEN_H
#define MARTIEN_H

#include "ExtraTerrestre.h"

class Martien : public ExtraTerrestre
{
	bool jiggle;

public:
	Martien(int,int);
	void jiggleMartien();
};

#endif


/* VERSION DE MENG #pragma once
#include "MethodesCommunes.h"
#include "Balle.h"

#define largeurMartien 12
#define hauteurMartien 22
#define MAX_BALLES 80

class Martien
{
private:
	static int vitesse;

	int posX;
	int posY;
	int debutDeplacer;
	int debutTirer;
	Balle* balles[MAX_BALLES];
	int nbBalles;

public:
	//constructeur sans parametre
	Martien();
	//constructeur
	Martien(int posX, int posY, int clock);

	int getPosX() const;
	int getPoxY() const;

	void setPosX(int posX);
	void setPosY(int posY);

	void afficher() const;
	void effacer() const;

	void deplacer(bool horizontal);
	void creerBalle(int agressivite);

	void setVitesse(int vitesse);

};


int Martien::vitesse = 100;*/