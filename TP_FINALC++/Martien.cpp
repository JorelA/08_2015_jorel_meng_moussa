#include "Martien.h"

Martien::Martien(int type,int valeur):ExtraTerrestre(type,valeur)
{
	jiggle = true;
}

void Martien::jiggleMartien()
{
	removeExtraTerrestre();
	
	if (jiggle)
		coord.setPositionX(coord.getPositionX()-1);
	else
		coord.setPositionX(coord.getPositionX()+1);

	jiggle = !jiggle;
	
	putExtraTerrestre();
}

/* VERSION DE MENG #include "Martien.h"
#include <string>
#include "UIkit.h"
#include <iostream>
#include <conio.h>
using namespace std;

//constructeur sans parametre
Martien::Martien(){}
//constructeur
Martien::Martien(int posX, int posY, int clock) {
	this->posX = posX;
	this->posY = posY;
	this->debutDeplacer = clock;
	this->debutTirer = clock;
	this->nbBalles = 0;
}

void Martien::setPosX(int posX) {
	this->posX = posX;
}

void Martien::setPosY(int posY) {
	this->posY = posY;
}

void Martien::afficher() const{
	UIKit::gotoXY(posX, posY);

	cout << "  _|_ " << endl;
	cout << "--(*)--" << endl;
	cout << " \" ' \"" << endl;
}

//void Martien::effacer() const{
//	for (int i = 0; i < hauteurMartien; i++){
//		UIKit::gotoXY(posX, posY + i);
//		for (int j = 0; j < largeurMartien; j++)
//			cout << " ";
//	}
//}

void Martien::deplacer(bool estHorizontal){
	// 1. attendre
	if (clock() - debutDeplacer >= vitesse){
		//reset temps de depart
		debutDeplacer = clock();

		// 2.  effacer martien
		for (int i = 0; i < hauteurMartien; i++){
			UIKit::gotoXY(posX, posY + i);
			for (int j = 0; j < largeurMartien; j++)
				cout << " ";
		}

		// 3.  modifier la position X et Y (horizontal ou vertical)
		if (estHorizontal){
			//valeur aleatoire pour determiner un deplacement vers gauche ou droite
			int gauche_droite = rand() % 2;
			//bordure de gauche et de droite
			bool gauchInterdit = posX - 1 == 2 || UIKit::getCharXY(posX - 1, posY) != ' ';
			bool droiteInterdit = posX + 1 == 80 || UIKit::getCharXY(posX + 1, posY) != ' ';

			if (gauchInterdit && !droiteInterdit)
				posX++;
			else if (!gauchInterdit && droiteInterdit)
				posX--;
			else if (gauche_droite == 0 && !gauchInterdit)
				posX--;
			else if (gauche_droite == 1 && !droiteInterdit)
				posX++;
		}
		else
			posY++; //movement vertical
		//pour la prochaine fois
		estHorizontal = !estHorizontal;

		// 4.  re-afficher
		afficher();
	}
}

//movement des balles d'un martien
void Martien::creerBalle(int agressivite) {
	if (clock() - debutTirer >= agressivite){
		//reset temps de depart
		debutTirer = clock();
		balles[nbBalles] = new Balle(posX, posY, clock());
		nbBalles++;
	}
}

void Martien::setVitesse(int vitesse){
	//modifier la variable static
	Martien::vitesse = vitesse;
}*/