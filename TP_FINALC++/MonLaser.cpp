#include "MonLaser.h"
#include "UIkit.h"
#include <iostream>
using namespace std;
#include "MonMartien.h"
#include <time.h>
#include "Coord.h"

MonLaser::MonLaser() {
}

void MonLaser::init(int posX, int posY, int type) {
	coord.setPositionX(posX);
	coord.setPositionY(posY);
	this->type = type;
	isAlive = false;
}

MonLaser::~MonLaser() {
}

int MonLaser::getPosX() {
	return coord.getPositionX();
}

int MonLaser::getPosY() {
	return coord.getPositionY();
}

void MonLaser::deplacer() {
		
		UIKit::gotoXY(getPosX(), getPosY());
		cout << "  ";

	if (getPosY() < 40) {
		//nouvelle position
		coord.setPositionY(coord.getPositionY() + 1);

		//afficher
		UIKit::gotoXY(getPosX(), getPosY());

		if (type == 0)
			UIKit::color(2); // defini dans MonMartien.h
		else
			UIKit::color(4); // defini dans MonMartien.h
		cout << "|";
	}
	else
		setIsAlive(false);
	
		
}

void MonLaser::setIsAlive(bool etat) {
	isAlive = etat;
}

bool MonLaser::getIsAlive() {
	return isAlive;
}