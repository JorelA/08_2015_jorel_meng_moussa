#include "MonMartien.h"
#include <iostream>
#include <time.h>
#include <conio.h>
#include "UIkit.h"
#include "MonLaser.h"
#include "Menu.h"
using namespace std;

#define LIMITE_GAUCHE 2

MonMartien::MonMartien(int type, int valeur, int delayMouvement) : Martien(type, valeur) {
	debutMouvement = clock();
	this->delayMouvement = delayMouvement;
	deplacer = true;

	delayBoucheOuerte = 500;
	debutBoucheOuerte = clock();
	boucheOuverte = true;
}

MonMartien::~MonMartien()
{
}

//Redefinition la methode de ExtraTerrestre
void MonMartien::removeExtraTerrestre() const {
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << "    ";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << "    ";
}

void MonMartien::PutExtraTerrestre() {
	/*
	if (typeExtraTerrestre == 0){ // le type 0
	if (clock() - debutBoucheOuerte >= delayBoucheOuerte){ //	delay du mouvement de bouche;
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	// premiere ligne le  martien
	cout << "{@@}";
	// deuxieme ligne le  martien
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	if (boucheOuverte)
	cout << "/\"\"\\";
	else
	cout << " \\/" << endl;
	debutBoucheOuerte = clock();
	boucheOuverte = !boucheOuverte;
	}
	}
	*/

	if (typeExtraTerrestre == 0) { // le type 0
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "{@@}";
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		if (clock() - debutBoucheOuerte >= delayBoucheOuerte) {//	delay du mouvement de bouche;
			cout << "/\"\"\\";
			debutBoucheOuerte = clock();
		}
		else
			cout << " \\/" << endl;

		//boucheOuverte = !boucheOuverte;
	}
	else {
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		// premiere ligne le  martien
		cout << "/MM\\" << endl;
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		// deuxieme ligne le  martien
		if (clock() - debutBoucheOuerte >= delayBoucheOuerte) {
			cout << "|~~|" << endl;
			debutBoucheOuerte = clock();
		}
		else
			cout << "\\~~/" << endl;
	}



	/*
	else{ // le  type 1
	if (clock() - debutBoucheOuerte >= delayBoucheOuerte){ //	delay du mouvement de bouche;
	// premiere ligne le  martien
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << "/MM\\" << endl;
	// deuxieme ligne le  martien
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);

	if (boucheOuverte)
	cout << "|~~|" << endl;
	else
	cout << "\\~~/" << endl;
	debutBoucheOuerte = clock();
	boucheOuverte = !boucheOuverte;
	}
	}
	*/
}

//etinceller d'un martien
void MonMartien::afficher() {
	if (typeExtraTerrestre == 0) { // le type 0
		if (clock() - debutBoucheOuerte >= delayBoucheOuerte) { //	delay du mouvement de bouche;
			removeExtraTerrestre(); // effacer le martien
			coord.gotoXY(coord.getPositionX(), coord.getPositionY());
			// premiere ligne le  martien
			cout << "{@@}";
			coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
			// deuxieme ligne le  martien
			if (boucheOuverte)
				cout << "/\"\"\\";
			else
				cout << " \\/" << endl;
			debutBoucheOuerte = clock();
			boucheOuverte = !boucheOuverte;
		}
	}
	else { // le  type 1
		if (clock() - debutBoucheOuerte >= delayBoucheOuerte) { //	delay du mouvement de bouche;
			removeExtraTerrestre();// effacer le martien
			coord.gotoXY(coord.getPositionX(), coord.getPositionY());
			// premiere ligne le  martien
			cout << "/MM\\" << endl;
			coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
			// deuxieme ligne le  martien
			if (boucheOuverte)
				cout << "|~~|" << endl;
			else
				cout << "\\~~/" << endl;
			debutBoucheOuerte = clock();
			boucheOuverte = !boucheOuverte;
		}
	}


}

void MonMartien::afficherOuvert() {
	if (typeExtraTerrestre == 0) {
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "{@@}";
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		cout << "/\"\"\\";
	}
	else {
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "/MM\\" << endl;
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		cout << "|~~|" << endl;
	}
	coord.setPositionY(coord.getPositionY());
}

void MonMartien::afficherFerme() {
	if (typeExtraTerrestre == 0) {
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "{@@}";
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		cout << " \\/" << endl;
	}
	else {
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "/MM\\" << endl;
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		cout << "\\~~/" << endl;
	}
	coord.setPositionY(coord.getPositionY());
}

void MonMartien::jiggleMartien()
{
	// 1. attendre

		/*UIKit::gotoXY(coord.getPositionX()-1, coord.getPositionY());
		cout << "G";
		UIKit::gotoXY(coord.getPositionX()-1, coord.getPositionY() + 1);
		cout << "G";

		UIKit::gotoXY(coord.getPositionX()+4, coord.getPositionY());
		cout << "D";
		UIKit::gotoXY(coord.getPositionX()+4, coord.getPositionY() + 1);
		cout << "D";

		UIKit::gotoXY(coord.getPositionX() + 1, coord.getPositionY()+2);
		cout << "B";
		UIKit::gotoXY(coord.getPositionX() + 2, coord.getPositionY() + 2);
		cout << "B";
		UIKit::gotoXY(coord.getPositionX() + 3, coord.getPositionY()+2);
		cout << "B";
		UIKit::gotoXY(coord.getPositionX(), coord.getPositionY() + 2);
		cout << "B";*/
		// 2.  effacer martien
		//removeExtraTerrestre();

		// 3.  modifier la position X et Y (horizontal ou vertical)
		int posX = coord.getPositionX();
		int posY = coord.getPositionY();
		if (deplacer) {
			//valeur aleatoire pour determiner un deplacement vers gauche ou droite
			int direction = rand() % 3;
			//bordure de gauche et de droite
			bool gauchInterdit = UIKit::getCharXY(posX - 1, posY) != 32 || UIKit::getCharXY(posX - 1, posY + 1) != 32;

			bool droiteInterdit = UIKit::getCharXY(posX + 4, posY) != 32 || UIKit::getCharXY(posX + 4, posY + 1) != 32;

			bool basInterdit = UIKit::getCharXY(posX + 1, posY + 2) != 32 || UIKit::getCharXY(posX + 2, posY + 2) != 32 ||
				UIKit::getCharXY(posX + 3, posY + 2) != 32 || UIKit::getCharXY(posX, posY + 2) != 32;

			switch (direction) {
			case 0:
				if (!gauchInterdit) {

					UIKit::gotoXY(posX + 3, posY);
					cout << " ";
					UIKit::gotoXY(posX + 2, posY + 1);
					cout << " ";
					UIKit::gotoXY(posX + 3, posY + 1);
					cout << " ";
					coord.setPositionX(posX - 1);
				}
				break;
			case 1:
				if (!droiteInterdit) {

					UIKit::gotoXY(posX, posY);
					cout << " ";
					UIKit::gotoXY(posX, posY + 1);
					cout << " ";
					coord.setPositionX(posX + 1);
				}
				break;
			case 2:
				if (!basInterdit) {

					UIKit::gotoXY(posX, posY);
					cout << " ";
					UIKit::gotoXY(posX + 1, posY);
					cout << " ";
					UIKit::gotoXY(posX + 2, posY);
					cout << " ";
					UIKit::gotoXY(posX + 3, posY);
					cout << " ";
					coord.setPositionY(posY + 1);
				}
				break;
			default:
				break;
			}

			//if(!basInterdit && clock() % 11 == 0)
			//coord.setPositionY(posY + 1);
		}

		deplacer = !deplacer;

		// 4.  re-afficher
		PutExtraTerrestre();
}

void MonMartien::initLaser(int x, int y, int type) {
	this->unLaser.init(x, y, type);
	unLaser.setIsAlive(true);
}

void MonMartien::feu() {
		this->unLaser.deplacer();
}

bool MonMartien::getLaserStatus() {
	return unLaser.getIsAlive();
}

void MonMartien::killLaser() {
	unLaser.setIsAlive(false);
}




/*
removeExtraTerrestre();

if (jiggle)
coord.setPositionX(coord.getPositionX() - 1);
else
coord.setPositionX(coord.getPositionX() + 1);

jiggle = !jiggle;

putExtraTerrestre();
};
*/