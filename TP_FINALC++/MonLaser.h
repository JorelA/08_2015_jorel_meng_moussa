#pragma once

#include "Coord.h"

class MonLaser
{
private:
	int type;
	bool isAlive;
public:
	Coord coord;

	MonLaser();

	~MonLaser();

	void init(int posX, int posY, int type);

	int getPosX();
	int getPosY();

	void deplacer();

	void setIsAlive(bool);
	bool getIsAlive();
};
