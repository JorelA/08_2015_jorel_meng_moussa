#pragma once
#include "Menu.h"
#include "SousMenu.h"
#include <string>
#include <iostream>
#include <time.h>
#include <fstream>
#include "UIkit.h"
using namespace std;

//CONSTRUCTEUR NON PARAM�TR�
Menu::Menu() {
	maxOptions = 1;
	options = new string[maxOptions];
	nbOptions = 0;
	selectedOption = 0;
	nombre = 15;
	vitesse = 500;
	aggro = 300;
}

//CONSTRUCTEUR PAR RECOPIE
Menu::Menu(Menu &Menu) {
	this->maxOptions = Menu.maxOptions;
	this->nbOptions = Menu.nbOptions;
	this->selectedOption = Menu.selectedOption;
}

//M�thode qui AJOUTE une option au tableau dynamique d'options ( en g�rant la taille du tableau )
void Menu::addOption(string newOpt) {
	if (nbOptions == maxOptions) {

		string *temp = new string[maxOptions * 2];

		for (int i = 0; i < nbOptions; i++)
			temp[i] = options[i];

		delete[]options;
		options = temp;
		maxOptions *= 2;
	}

	options[nbOptions] = newOpt;
	nbOptions++;
}

//M�thode qui SUPPRIME une option du tableau dynamique d'options ( en g�rant la taille du tableau )
void Menu::removeOption(int indice) {
	for (int i = indice - 1; i < nbOptions; i++) {
		options[i] = options[i + 1];
	}
	nbOptions--;

	if (nbOptions <= maxOptions / 2) {
		maxOptions /= 2;
		string *temp = new string[maxOptions];

		for (int i = 0; i < nbOptions; i++)
			temp[i] = options[i];

		delete[]options;
		options = temp;
	}
}

//M�thode qui SUPPRIME TOUT le tableau dynamique d'options ( remise � z�ro )
void Menu::clearOption() {
	delete[]options;
	maxOptions = 1;
	options = new string[maxOptions];
	nbOptions = 0;
	selectedOption = 0;
}

//M�thode qui affiche en couleur une ligne en fonction des fl�ches UP and DOWN pr�ss�es
int Menu::navigationMenu() {

	//On affiche en couleur le menu en passant selectedOption comme param�tre, ce qui permettra d'afficher en couleur
	//l'options[selectedOption] ( selectedOption = 0 par defaut, c'est donc options[0] qui sera color� par defaut, soit la 1ere ligne )
	afficherCouleur(selectedOption, 68, 22);

	//On recup�re la saisie clavier dans le key
	char key = _getch();

	//Si c'est la fleche du haut et que on est pas au d�but
	if (key == FLECHE_HAUT && selectedOption != 0) {

		//On diminue selectedOption
		selectedOption--;
		
	}

	//Sinon si c'est la fleche du bas et que on est pas a la fin
	else if (key == FLECHE_BAS && selectedOption != nbOptions - 1) {

		//On augmente selectedOption
		selectedOption++;
	}

	//Sinon si cest la fleche de droite
	else if (key == FLECHE_DROITE) {

		//On cr�e une instance de la classe sous Menu
		SousMenu unSousMenu;

		//On rentre dans la boucle de navigation du sous menu
		//Tant que celle ci ne renvoi par l'int correspondant a la touche ENTER, on reste dedans
		while (unSousMenu.navigationSousMenu(getSelectedOption()) != FLECHE_GAUCHE);

		//Apr�s le sous menu, efface la zone cr�e par le sous menu
		effacerZone(18,31,89,20);

		setAggro(unSousMenu.getAggro());
		setVitesse(unSousMenu.getVitesse());
		setNombre(unSousMenu.getNombre());

	}
	return key;

}

//M�thode qui regroupe les �l�ments d'affichage visuel 
void Menu::loadingScreen() {
	afficherItem("SI",3, 3);
	afficherSpaceInvaders();

	UIKit::cadre(65, 17, 87, 19, 2);
	UIKit::gotoXY(67, 18);
	cout << "     MENU";
	//On cr�er un cadre "responsive" qui s'agrandie en fonction du nombre d'options
	UIKit::cadre(65, 20, 87, getNbOptions() * 6 + 2, 4);
}

//M�thode qui affiche le II de space invaderse ( cout de gauche a droite, puis de droite a gauche, puis de gauche a droite ... )
void Menu::afficherSpaceInvaders() {

	//Multiples boucles allant de gauche a droite et en sautant une ligne apr�s chaque afin d'animer l'affichage du II
	int ligne = 110;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 3);
		cout << " ______  ______     " << " " << endl;
		attendre(20);
		ligne--;
	}

	ligne = 90;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 4);
		cout << " " << " /\\__  _\\/\\__  _\\    " << endl;
		attendre(20);
		ligne++;
	}

	ligne = 110;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 5);
		cout << "\\/_/\\ \\/\\/_/\\ \\/    " << " " << endl;
		attendre(20);
		ligne--;
	}

	ligne = 90;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 6);
		cout << " " << "    \\ \\ \\   \\ \\ \\    " << endl;
		attendre(20);
		ligne++;
	}

	ligne = 110;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 7);
		cout << "    \\_\\ \\__ \\_\\ \\__ " << " " << endl;
		attendre(20);
		ligne--;
	}

	ligne = 90;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 8);
		cout << " " << "     /\\_____\\/\\_____\\ " << endl;
		attendre(20);
		ligne++;
	}

	ligne = 110;

	for (int i = 0; i < 10; i++) {
		UIKit::gotoXY(ligne, 9);
		cout << "    \\/_____/\\/_____/ " << " " << endl;
		attendre(50);
		ligne--;
	}
}

//M�thode qui recuperer un fichier.txt dans le root du dossier et l'affiche a la position voulue
void Menu::afficherItem(string filename, int posX, int posY) {

	//On cr�e un string
	string ascii;

	//On cr�er un ifstream ( variable qui stocke le contenu d'un fichier )
	ifstream Reader;

	//On ouvre le fichier gr�ce a son nom pass� en param�tre. DOIT �TRE UN .TXT pr�sent dans le root du TP
	Reader.open(filename+".txt");
	
	//On boucle tant que on a pas fini de lire le fichier ( tant que NOT Reader.EndOfFile() )
	while (!Reader.eof())
	{
		//On va aux poisitions pass�s en param�tre
		UIKit::gotoXY(posX, posY);

		//On met la ligne du fichier ouvert dans la variable ascii
		getline(Reader, ascii);
		
		//On affiche la variable
		cout << ascii << endl;

		//On saute une ligne pour afficher
		posY++;
	}

	//On se place au m�me poisition initial -24 afin d'effacer le BOM, bytes order markes, qui sont 3 caract�re g�nant pour l'affichage au
	//d�but du fichier
	UIKit::gotoXY(posX, posY - 24);
	cout << " " << " " << " ";

	//On ferme le fichier
	Reader.close();
}

//M�thode qui attends un nombre de millisecondes
void Menu::attendre(int milliseconde) {
	int debut = clock();

	//On reste dans la boucle dans qu'on a pas atteint le nombre de millisecondes souhait�es
	while (clock() < debut + milliseconde);
}

void Menu::effacerZone(int nb_colonnes_a_effacer, int nb_ligne_a_effacer, int posX, int posY) {
	for (int i = 0; i <= nb_colonnes_a_effacer; i++)
		for (int j = 0; j <= nb_ligne_a_effacer; j++) {
			UIKit::gotoXY(posX+j, posY+i);
			cout << " ";
		}
}

//M�thode qui affiche la ligne numero SelectedOption en couleur
void Menu::afficherCouleur(int optionCouleur, int posX, int posY) {
	//x68y22
	//Positions a l'interieur du cadre
	int colonne_depart = posY;
	int ligne_depart = posX;
	
	//On change la couleur
	UIKit::color(2);

	int i = 0;

	//De la 1ere option, jusqu'avant celle de en couleur, on affiche
	for (i = 0; i < optionCouleur; i++) {
		UIKit::gotoXY(ligne_depart, colonne_depart);
		cout << options[i] << "  " << endl;

		//Chaque tour on saute 2 lignes
		colonne_depart += 2;
	}

	//On change de couleur
	UIKit::color(4);

	//On se place � la poisitien suivante
	UIKit::gotoXY(ligne_depart, colonne_depart);

	//On affiche l'options a l'indice optionCouleur pass� en param�tre
	cout << options[optionCouleur] << " > " << endl;

	//On saute 2 lignes
	colonne_depart += 2;

	//On change de couleur
	UIKit::color(2);

	//De l'option en couleur +1 jusqu'au d'options total, on affiche
	for (i = optionCouleur + 1; i < nbOptions; i++) {
		UIKit::gotoXY(ligne_depart, colonne_depart);
		cout << options[i] << "  " << endl;
		colonne_depart += 2;
	}
}



//ACCESSEUR DE MAXOPTION
int Menu::getMaxOption() {
	return maxOptions;
}

//ACCESSEUR DE NB OPTION
int Menu::getNbOptions() {
	return nbOptions;
}

//ACCESSEUR DE OPTIONS
string  Menu::getOptions(int indice) {
	return options[indice];
}

//ACCESSEUR DE SELECTED OPTION
int  Menu::getSelectedOption() {
	return selectedOption;
}

//ACCESEUR DE NOMBRE
int Menu::getAggro() {
	return aggro;
}

int Menu::getVitesse() {
	return vitesse;
}

int Menu::getNombre() {
	return nombre;
}

//MUTATEUR DE NOMBRE
void Menu::setAggro(int aggro) {
	this->aggro = aggro;
}

//MUTATEUR DE VITESSE
void Menu::setVitesse(int vitesse) {
	this->vitesse = vitesse;
}

//MUTATEUR DE NOMBRE
void Menu::setNombre(int nombre) {
	this->nombre = nombre;
}


//DESTRUCTEUR
Menu::~Menu() {
	delete[] this->options;
}
		
