//#include "GroupeMartien.h"
//#include "ChaqueMartien.h"

#include <iostream>
#include <string>
#include "Menu.h"
#include "SousMenu.h"
#include <conio.h>
#include "UIkit.h"
#include <time.h>
#include <fstream>
#include "Vaisseau.h"
#include "Creer_Vaisseau.h"
#include "Laser.h"
#include "ExtraTerrestre.h"
#include "Martien.h"
#include "MonMartien.h"
#include "LesMartiens.h"
using namespace std;

#define ENTER 13
//#define MAX_LASERS 30
#define LIMITE_MARTIEN 39
#define MUR_HAUT 80
#define ESPACE 32
#define LE_LASER_DEVANT 15
#define POS_Y_DES_MORTS 60
#define AXE_Y_PRECEDANT_LE_MUR_DU_HAUT 1

int main() {

	Menu unMenu;
	UIKit::curseurVisible(false);
	UIKit::setDimensionFenetre(0, 100, 0, 100);

	//AJOUT D'OPTIONS
	unMenu.addOption("Commencer");
	unMenu.addOption("Difficulte");
	unMenu.addOption("Scores");
	unMenu.addOption("Options avancee");
	unMenu.addOption("Exit");

	//On affiche les �l�ments visuel
	unMenu.loadingScreen();

	//Tant que l'utilisateur n'a pas appuy� sur entrer ( unMenu.navigation == 10 ) ou qu'il n'est sur la premiere option
	//(selectedOoptions == 0 ) lorsque il appuie sur enter, on boucle sur la navigation
	//(Si il appuie sur enter sur la premiere option, le jeu devra d�marrer, on quittera la navigation );
	while (unMenu.navigationMenu() != ENTER || unMenu.getSelectedOption() != 0)
		unMenu.navigationMenu();

	system("cls");
	//cout << "nombre : " << unMenu.getNombre() << endl;
	/*cout << "aggro : " << unMenu.getAggro() << endl;
	cout << "nombre : " << unMenu.getNombre() << endl;
	cout << "vitesse : " << unMenu.getVitesse() << endl;*/

	Vaisseau unVaisseau;
	Laser tabLaser[MAX_LASERS];
	int nbMartiens = unMenu.getNombre(); // A MODIFIER SELON LES PARAM�TREs

	int score = 0;
	int nbLaser = 0;
	int attente = 0;
	LesMartiens lesmartiens(clock(), 300, 400, 200, nbMartiens);
	bool UnMartienEnBas = false;
	while (nbMartiens > 0 && UnMartienEnBas == false) {
		attente++;
		UIKit::cadre(0, 0, 70, 50, FOREGROUND_BLUE + BACKGROUND_INTENSITY + 24);
		UIKit::color(2);
		lesmartiens.afficherDesMartiens();
		if (attente % 15 == 0)
			lesmartiens.jiggleLesMartiens();
		UIKit::gotoXY(70, 40);
		cout << "Score : " << score << " nb martien " << nbMartiens << endl;
		if (_kbhit()) {
			int touche = _getch();

			if (touche == ' ') {
				if (nbLaser < MAX_LASERS) {
					tabLaser[nbLaser].startLaser(unVaisseau.coord.getPositionX());
					nbLaser++;
				}
			}
			else
				if (unVaisseau.coord.getPositionX() > 1 && touche == 'k' || unVaisseau.coord.getPositionX() < 66 && touche == 'l')
					unVaisseau.modifierPosition(touche);
		}

		int a = 0;

		while (a < unMenu.getNombre()) {
			if (lesmartiens.getIsAlive(a) == true && lesmartiens.getPosY(a) >= LIMITE_MARTIEN && lesmartiens.getPosY(a) != POS_Y_DES_MORTS) {
				UnMartienEnBas = true;
			}
			a++;
		}
		int i = 0;
		int martien_random;

		if (nbMartiens != 0)
			martien_random = rand() % nbMartiens;

		if (attente % 100 == 0) {
			lesmartiens.preparerLesLaser(martien_random);
		}
		
		lesmartiens.susAuxEnnemis(martien_random);

		while (i < nbLaser) {
			if (attente % 5 == 0)
				tabLaser[i].moveLaser();

			int LaserCurrentX = tabLaser[i].coord.getPositionX();
			int LaserCurrentY = tabLaser[i].coord.getPositionY();

			char collision = UIKit::getCharXY(LaserCurrentX, LaserCurrentY - 1);

			if (collision != ESPACE && collision != MUR_HAUT && collision != LE_LASER_DEVANT) {
				lesmartiens.removeMartien(LaserCurrentX, LaserCurrentY - 1, nbMartiens, score);
				tabLaser[i].killLaser();
			}
			else if (LaserCurrentY == AXE_Y_PRECEDANT_LE_MUR_DU_HAUT) {
				tabLaser[i].killLaser();
			}

			if (!tabLaser[i].isAlive) {
				tabLaser[i] = tabLaser[nbLaser - 1];
				nbLaser--;
			}
			else
				i++;
		}

	}

	if (nbMartiens = 10)
		cout << "VOUS AVEZ TUER TOUT LES MARTIENS !";
	else
		cout << "UN MARTIEN EST ARRIVE DANS LA BASE";


	/*cout << "aggro : " << unMenu.getAggro() << endl;

	cout << "vitesse : " << unMenu.getVitesse() << endl;*/

	return 0;
}

