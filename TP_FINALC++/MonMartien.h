#pragma once
#include "Martien.h"
#include "Coord.h"
#include "MonLaser.h"

#define LARGEUR_Martien 4
#define HAUTEUR_Martien 2

class MonMartien : public Martien
{
private:
	int debutMouvement;
	int delayMouvement;
	bool deplacer;

	int delayBoucheOuerte;
	int debutBoucheOuerte;
	bool boucheOuverte;
	bool estVivant;
	MonLaser unLaser;
public:
	int posXMartiens;
	int posYMartiens;

	Coord coord;
	MonMartien(int type, int valeur, int delayMouvement);
	~MonMartien();

	void removeExtraTerrestre() const;

	//etinceller d'un martien
	void afficher();
	void PutExtraTerrestre();

	void jiggleMartien();

	void afficherOuvert();
	void afficherFerme();

	void initLaser(int x, int y, int type);
	void feu();
	bool getLaserStatus();
	void killLaser();
};

