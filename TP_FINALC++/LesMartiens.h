#pragma once
#include "MonMartien.h"
#include <time.h>
#include "Coord.h"


#define MAX_LASERS 80

class LesMartiens {
private:
	//int debutMouvement;
	int delayMouvement;

	int delayNouveauMartien;
	int debutNouveauMartien;

	int delayTirer;
	int debutTirer;

	int delayBoucheOuerte;
	int debutBoucheOuerte;
	bool boucheOuverte;

	MonMartien** tbMonMartiens;
	int nbMartien;

public:
	Coord coord;
	LesMartiens(int clock, int delayMouvement,
		int delayNouveauMartien, int delayTirer, int nbMartien);
	~LesMartiens();

	void afficherDesMartiens();

	void removeMartien(int posX, int posY, int &nbMartiens, int &score);

	void jiggleLesMartiens();

	void preparerLesLaser(int indiceMartien);
	void susAuxEnnemis(int indiceMartien);
	bool rechargerLesLaser(int nbMartiens);
	int getPosY(int indice);
	bool getIsAlive(int indice);
};