#ifndef LASER_H
#define LASER_H

#include "Coord.h"

class Laser
{
public:
	bool isAlive;
	Coord coord;

	void startLaser(int);
	void removeLaser() const;
	void putLaser() const;
	virtual void moveLaser();
	void killLaser();
};

#endif

/*VERS DE MENG

#pragma once

#define VITESSE_BALLE 10

class Balle
{
private:
int static agressivite;

int posX;
int posY;
int debut;  // depart du movement de balle

public:
Balle(int posX_Martien, int posY_Martien, int clock);
~Balle();

void deplacer();

void setAgressivite(int agressivite);
};

int Balle::agressivite = 200;*/