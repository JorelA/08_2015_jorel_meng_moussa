#include <string>
#include <iostream>
#include <time.h>
#include <conio.h>
#include <fstream>
#include "UIkit.h"
using namespace std;

void afficherItem(string filename, int posX, int posY) {

	//On cr�e un string
	string ascii;

	//On cr�er un ifstream ( variable qui stocke le contenu d'un fichier )
	ifstream Reader;

	//On ouvre le fichier gr�ce a son nom pass� en param�tre. DOIT �TRE UN .TXT pr�sent dans le root du TP
	Reader.open(filename + ".txt");

	//On boucle tant que on a pas fini de lire le fichier ( tant que NOT Reader.EndOfFile() )
	while (!Reader.eof())
	{
		//On va aux poisitions pass�s en param�tre
		UIKit::gotoXY(posX, posY);

		//On met la ligne du fichier ouvert dans la variable ascii
		getline(Reader, ascii);

		//On affiche la variable
		cout << ascii << endl;

		//On saute une ligne pour afficher
		posY++;
	}

	//On se place au m�me poisition initial -24 afin d'effacer le BOM, bytes order markes, qui sont 3 caract�re g�nant pour l'affichage au
	//d�but du fichier
	UIKit::gotoXY(posX, posY - 24);
	cout << " " << " " << " ";

	//On ferme le fichier
	Reader.close();
}

void attendre(int milliseconde) {
	int debut = clock();

	//On reste dans la boucle dans qu'on a pas atteint le nombre de millisecondes souhait�es
	while (clock() < debut + milliseconde);
}