#include "Laser.h"

#include <iostream>
using namespace std;

void Laser::startLaser(int x)
{
	coord.setPositionX(x);
	coord.setPositionY(39);
	putLaser();
	isAlive = true;
}

void Laser::removeLaser() const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << " ";
}

void Laser::putLaser() const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << char(15);
}

void Laser::moveLaser()
{
	removeLaser();
	if (coord.getPositionY()>0)
	{
		coord.setPositionY(coord.getPositionY() - 1);
		putLaser();
	}
	else
		isAlive = false;
}

void Laser::killLaser()
{
	removeLaser();
	isAlive = false;
}

/*version balle de meng

#include "Balle.h"
#include "MethodesCommunes.h"

Balle::Balle(int posX_Martien, int posY_Martien, int clock){
posX = posX_Martien + 3;  //position est selon la position du martien
posY = posY_Martien + 4;  //position est selon la position du martien
debut = clock;
}


Balle::~Balle()
{
}

void Balle::deplacer() {
UIKit::gotoXY(posX, posY);
cout << "|";

if (clock() - debut >= VITESSE_BALLE){
debut = clock();//reset le debut
UIKit::gotoXY(posX, posY);
cout << " ";
posY++;
UIKit::gotoXY(posX, posY);
cout << "|";
}
}

void Balle::setAgressivite(int agressivite){
//modifier la variable static
Balle::agressivite = agressivite;
}*/