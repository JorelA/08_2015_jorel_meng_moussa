#include "LesMartiens.h"
#include "MonMartien.h"
#include "UIkit.h"
#include <conio.h>
#include <fstream>
#include <time.h>
#include "Coord.h"

//#include "Laser.h"

#define NB_MARTIEN 10
#define Y_PREMIER_LIGNE 5
#define Y_DEUXIEME_LIGNE 8
LesMartiens::LesMartiens(int clock, int delayMouvement, int delayNouveauMartien,
	int delayTirer, int nbMartien) {

	//debutMouvement = clock;
	debutNouveauMartien = clock;
	debutTirer = clock;

	debutBoucheOuerte = clock;
	delayBoucheOuerte = 150;
	boucheOuverte = true;

	this->delayMouvement = delayMouvement;
	this->delayNouveauMartien = delayNouveauMartien;
	this->delayTirer = delayTirer;


	this->nbMartien = nbMartien;
	//tableau des adresses des martiens
	tbMonMartiens = new MonMartien*[nbMartien];
	int j = 0;
	int posY = 0;
	//initialisatin des martiens de premiere partie

	for (int i = 0; i < this->nbMartien; i++) {
		

		//if (i == 0)
		//	tbMonMartiens[i]->coord.setPositionX(10);
		//else  // espace entre les 2 martiens: 8
		//	tbMonMartiens[i]->coord.setPositionX(i * 12 + 4);
		if (i % 5 == 0) {
			posY += 3;
			j = 0;
		}

		int type;

		if (i >= 5 && i < 10 || i >= 15 && i < 20 || i >= 25 && i < 30)
			type = 0;
		else
			type = 1;

			tbMonMartiens[i] = new MonMartien(type, 250, delayMouvement); //type:0,  valeur:1 point
			
																	   //tb
		tbMonMartiens[i]->coord.setPositionY(posY);
		tbMonMartiens[i]->coord.setPositionX(j * 12 + 8);
		j++;
		tbMonMartiens[i]->isAlive = true;

	}


	//initialisatin des martiens de deuxieme partie
	//int martienReste = nbMartien - 10;
	//if (martienReste > 0) {
	//	srand((unsigned int)time(NULL));
	//	int type;
	//	for (int i = 0; i < martienReste; i++) {
	//		type = rand() % 2;
	//		// valeur = type + 1
	//		tbMonMartiens[i] = new MonMartien(type, type + 1, delayMouvement);
	//		tbMonMartiens[i]->isAlive = false;
	//		bool refaire = true;
	//		do {  //si il y a une chose a droite de martien
	//			tbMonMartiens[i]->coord.setPositionX(rand() % 122);
	//			int j = 0;
	//			while (j < 4 && UIKit::getCharXY(tbMonMartiens[i]->coord.getPositionX() + j, tbMonMartiens[i]->coord.getPositionY()) == ' ')
	//				j++;
	//			if (j == 4) // il n'y a pas obstable a droite
	//				refaire = false;
	//		} while (refaire);
	//		tbMonMartiens[i]->coord.setPositionY(Y_PREMIER_LIGNE);
	//	}
	//}
}


LesMartiens::~LesMartiens() {
	for (int i = 0; i < nbMartien; i++)
		delete tbMonMartiens[i];
	delete[] tbMonMartiens;
}

void LesMartiens::afficherDesMartiens() {
	for (int i = 0; i < nbMartien; i++) {
		if (tbMonMartiens[i]->isAlive == true)
			tbMonMartiens[i]->afficher();
	}
}

void LesMartiens::jiggleLesMartiens() {
	for (int i = 0; i < nbMartien; i++)
		if (tbMonMartiens[i]->isAlive == true)
			tbMonMartiens[i]->jiggleMartien();
}

void LesMartiens::removeMartien(int posX, int posY, int &nbMartiens, int &score) {

	int i = 0;
	bool MartienTrouver = false;

	while (i < nbMartien && MartienTrouver == false) {
		if ( (tbMonMartiens[i]->coord.getPositionX() == posX && tbMonMartiens[i]->coord.getPositionY() == posY - 1)
			|| (tbMonMartiens[i]->coord.getPositionX() == posX-1 && tbMonMartiens[i]->coord.getPositionY() == posY - 1)
			|| (tbMonMartiens[i]->coord.getPositionX() == posX-2 && tbMonMartiens[i]->coord.getPositionY() == posY - 1)
			|| (tbMonMartiens[i]->coord.getPositionX() == posX-3 && tbMonMartiens[i]->coord.getPositionY() == posY - 1))
			MartienTrouver = true;
		else
			i++;
	}

	if (MartienTrouver) {
		tbMonMartiens[i]->killLaser();
		tbMonMartiens[i]->removeExtraTerrestre();
		tbMonMartiens[i]->isAlive = false;
		nbMartiens--;
		score += tbMonMartiens[i]->ajouterPoints();
		tbMonMartiens[i]->coord.setPositionX(0);
		tbMonMartiens[i]->coord.setPositionY(60);
		
	}


}

int LesMartiens::getPosY(int indice) {
	return tbMonMartiens[indice]->coord.getPositionY();
}

bool LesMartiens::getIsAlive(int indice) {
	return tbMonMartiens[indice]->isAlive;
}

void LesMartiens::preparerLesLaser(int indice) {
	tbMonMartiens[indice]->initLaser(tbMonMartiens[indice]->coord.getPositionX()+1, tbMonMartiens[indice]->coord.getPositionY()+1, tbMonMartiens[indice]->getTypeExtraTerrestre());
}

void LesMartiens::susAuxEnnemis(int indice) {
		tbMonMartiens[indice]->feu();
}

bool LesMartiens::rechargerLesLaser(int nbMartiens) {
	bool PlusDeLaserSurLeTerrain;
	int i = 0;
	while (i < nbMartien && tbMonMartiens[i]->getLaserStatus() == false)
		i++;

	if (i < nbMartien)
		PlusDeLaserSurLeTerrain = false;
	else
		PlusDeLaserSurLeTerrain = true;

	return PlusDeLaserSurLeTerrain;
};